package com.car.car.service.user;

import com.car.car.pojo.User;

import java.util.List;

public interface UserService {

    //根据loginCode获取用户信息
    User getUserByLoginCode(String loginCode) ;

    //获取所有用户
    List<User> getAllUsers() ;

    //根据用户id获取用户信息
    User getUserById(int uid) ;

    //添加用户
    int addUser(User user, int[] roleIds) ;
}
