package com.car.car.service.user.impl;

import com.car.car.dao.user.UserDao;
import com.car.car.pojo.User;
import com.car.car.service.user.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao ;

    @Override
    public User getUserByLoginCode(String loginCode) {
        return userDao.getUserByLoginCode(loginCode);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers() ;
    }

    @Override
    public User getUserById(int uid) {
        return userDao.getUserById(uid);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public int addUser(User user, int[] roleIds) {
        int success = 0 ;

        //先将用户加入用户表中
        userDao.addUser(user) ;

        //循环将用户选择的角色加入中间表
        for(int rid : roleIds) {
            userDao.addUserRole(user.getId(), rid) ;
        }


        success = 1 ;

        return success ;
    }
}
