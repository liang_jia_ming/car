package com.car.car.service.role.impl;

import com.car.car.dao.authority.AuthorityDao;
import com.car.car.dao.role.RoleDao;
import com.car.car.dao.user.UserDao;
import com.car.car.pojo.Role;
import com.car.car.service.role.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleDao roleDao ;

    @Resource
    private UserDao userDao ;

    @Resource
    private AuthorityDao authorityDao ;

    @Override
    public List<Role> getAllRoles() {
        return roleDao.getAllRoles();
    }

    @Override
    public Role getRoleInfoById(int rid) {
        return roleDao.getRoleInfoById(rid);
    }

    @Override
    public int addRole(Role role) {
        return roleDao.addRole(role);
    }

    @Override
    public int updateRole(Role role) {
        return roleDao.updateRole(role);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public int deleteRole(int rid) {
        int success = 0 ;

        //先删除角色下对应的用户角色中间表
        userDao.deleteUserRoleByRole(rid) ;


        //再删除角色下对应的权限中间表
        authorityDao.deleteAuthorityByRole(rid) ;


        //最后删除角色
        roleDao.deleteRole(rid) ;


        success = 1 ;

        return success;
    }
}
