package com.car.car.service.function.impl;

import com.car.car.dao.function.FunctionDao;
import com.car.car.pojo.Function;
import com.car.car.service.function.FunctionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FunctionServiceImpl implements FunctionService {

    @Resource
    private FunctionDao functionDao ;

    @Override
    public List<Function> getFirstFunctionList() {
        return functionDao.getFirstFunctionList();
    }

    @Override
    public List<Function> getChildrenFunctionList(int parentId) {
        return functionDao.getChildrenFunctionList(parentId);
    }

    @Override
    public Function getFunctionInfo(int id) {
        return functionDao.getFunctionInfo(id);
    }

    @Override
    public List<Function> getFunctionListByUser(int uid) {
        return functionDao.getFunctionListByUser(uid);
    }

    @Override
    public List<Function> getFunctionListByRole(int rid) {
        return functionDao.getFunctionListByRole(rid);
    }

    @Override
    public List<Function> getFirstMenuList() {
        return functionDao.getFirstMenuList();
    }

    @Override
    public List<Function> getSecondMenuList(int parentId) {
        return functionDao.getSecondMenuList(parentId);
    }

    @Override
    public List<Function> getAllFunctions(List<Function> functions, int parentId) {
        List<Function> functionList = functionDao.getChildrenFunctionList(parentId) ;

        functions.addAll(functionList) ;

        for(Function function : functionList) {
            this.getAllFunctions(functions, function.getId()) ;
        }

        return functions ;
    }

    @Override
    public int addFunction(Function function) {
        return functionDao.addFunction(function);
    }

    @Override
    public int updateFunction(Function function) {
        return functionDao.updateFunction(function);
    }

    @Override
    public int deleteFunction(int fid) {
        return 0;
    }
}
