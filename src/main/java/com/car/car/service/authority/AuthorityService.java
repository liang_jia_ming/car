package com.car.car.service.authority;

public interface AuthorityService {

    //根据角色添加授权
    int addAuthority(int rid, String functionIds) ;
}
