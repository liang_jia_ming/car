package com.car.car.service.authority.impl;

import com.car.car.dao.authority.AuthorityDao;
import com.car.car.pojo.Authority;
import com.car.car.service.authority.AuthorityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Resource
    private AuthorityDao authorityDao ;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public int addAuthority(int rid, String functionIds) {
        int success = 1 ;

        //先删除角色原先的权限
        authorityDao.deleteAuthorityByRole(rid) ;

        //再重新为角色添加权限
        for(String fid : functionIds.split(",")) {
            Authority authority = new Authority() ;
            authority.setRoleId(rid);
            authority.setFunctionId(Integer.parseInt(fid));

            authorityDao.addAuthority(authority) ;
        }

        return success ;
    }
}
