package com.car.car.service.statistics;

import com.car.car.pojo.RoleFunctionStatistic;

import java.util.List;

/**
 * 统计接口
 */
public interface StatisticsService {

    //统计角色拥有的权限数量
    List<RoleFunctionStatistic> getRoleFunctionCount() ;
}
