package com.car.car.service.statistics.impl;

import com.car.car.dao.statistics.StatisticsDao;
import com.car.car.pojo.RoleFunctionStatistic;
import com.car.car.service.statistics.StatisticsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

;

@Service
public class StatisticsServiceImpl implements StatisticsService {

    @Resource
    private StatisticsDao statisticsDao ;

    @Override
    public List<RoleFunctionStatistic> getRoleFunctionCount() {
        return statisticsDao.getRoleFunctionCount();
    }
}
