package com.car.car.service.menu.impl;

import com.car.car.pojo.Function;
import com.car.car.pojo.Menu;
import com.car.car.service.function.FunctionService;import com.car.car.service.menu.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private FunctionService functionService ;

    @Override
    public List<Menu> getMenuList() {
        List<Menu> menuList = new ArrayList<>() ;

        //先获取一级菜单
        List<Function> topFunction = functionService.getFirstMenuList() ;
        for(Function top : topFunction) {
            Menu menu = new Menu() ;
            menu.setTopFunc(top);

            //获取二级菜单
            List<Function> childrenFunction = functionService.getSecondMenuList(top.getId()) ;
            menu.setChildrenFunc(childrenFunction);

            menuList.add(menu) ;
        }

        return menuList ;
    }
}
