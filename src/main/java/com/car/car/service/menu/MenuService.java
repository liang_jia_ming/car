package com.car.car.service.menu;

import com.car.car.pojo.Menu;

import java.util.List;

/**
 * 菜单业务接口
 */
public interface MenuService {

    //获取系统的菜单
    List<Menu> getMenuList() ;
}
