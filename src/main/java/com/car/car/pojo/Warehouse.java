package com.car.car.pojo;


public class Warehouse {

  private int warehousenumber;
  private int parkingvolume;


  public int getWarehousenumber() {
    return warehousenumber;
  }

  public void setWarehousenumber(int warehousenumber) {
    this.warehousenumber = warehousenumber;
  }


  public int getParkingvolume() {
    return parkingvolume;
  }

  public void setParkingvolume(int parkingvolume) {
    this.parkingvolume = parkingvolume;
  }

  @Override
  public String toString() {
    return "Warehouse{" +
            "warehousenumber=" + warehousenumber +
            ", parkingvolume=" + parkingvolume +
            '}';
  }
}
