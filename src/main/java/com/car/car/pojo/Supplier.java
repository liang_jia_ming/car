package com.car.car.pojo;

import java.util.Date;

public class Supplier {

  private int sid;
  private String sname;
  private String saddress;
  private Date stime;
  private String phone;


  public int getSid() {
    return sid;
  }

  public void setSid(int sid) {
    this.sid = sid;
  }


  public String getSname() {
    return sname;
  }

  public void setSname(String sname) {
    this.sname = sname;
  }


  public String getSaddress() {
    return saddress;
  }

  public void setSaddress(String saddress) {
    this.saddress = saddress;
  }


  public Date getStime() {
    return stime;
  }

  public void setStime(java.sql.Date stime) {
    this.stime = stime;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public String toString() {
    return "Supplier{" +
            "sid=" + sid +
            ", sname='" + sname + '\'' +
            ", saddress='" + saddress + '\'' +
            ", stime=" + stime +
            ", phone='" + phone + '\'' +
            '}';
  }
}
