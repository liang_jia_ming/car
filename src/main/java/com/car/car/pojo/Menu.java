package com.car.car.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单类，用于存储一级菜单和二级菜单
 */
public class Menu implements Serializable {

    private Function topFunc ; //一级菜单
    private List<Function> childrenFunc ; //二级菜单

    public Function getTopFunc() {
        return topFunc;
    }

    public void setTopFunc(Function topFunc) {
        this.topFunc = topFunc;
    }

    public List<Function> getChildrenFunc() {
        return childrenFunc;
    }

    public void setChildrenFunc(List<Function> childrenFunc) {
        this.childrenFunc = childrenFunc;
    }
}
