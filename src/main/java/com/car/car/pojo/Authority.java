package com.car.car.pojo;

import java.io.Serializable;
import java.util.Date;

public class Authority implements Serializable {

    private int id ; //流水号
    private int roleId ; //角色id
    private int functionId ; //功能id
    private int userTypeId ; //用户类型
    private Date creationTime ; //创建时间
    private int createdBy ; //谁创建的

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getFunctionId() {
        return functionId;
    }

    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public String toString() {
        return "Authority{" +
                "id=" + id +
                ", roleId=" + roleId +
                ", functionId=" + functionId +
                ", userTypeId=" + userTypeId +
                ", creationTime=" + creationTime +
                ", createdBy=" + createdBy +
                '}';
    }
}
