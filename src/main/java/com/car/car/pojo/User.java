package com.car.car.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class User implements Serializable {

	private int id ;
	private String loginCode;
	private String password;
	private String salt;
	private String userName;
	private String sex;
	private Date birthday;
	private String cardType;
	private String cardTypeName;
	private String idCard;
	private String country;
	private String mobile;
	private String email;
	private String userAddress;
	private String postCode;
	private Date createTime;
	private String userType;
	private String userTypeName;
	private int isStart;
	private Date lastUpdateTime;
	private Date lastLoginTime;
	private String bankName;
	private String accountHolder;
	private String bankAccount;
	private String idCardPicPath;
	private String bankPicPath;

	private List<Role> roleList ; //用户对角色 1:n


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public String getLoginCode() {
		return loginCode;
	}

	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserTypeName() {
		return userTypeName;
	}

	public void setUserTypeName(String userTypeName) {
		this.userTypeName = userTypeName;
	}

	public int getIsStart() {
		return isStart;
	}

	public void setIsStart(int isStart) {
		this.isStart = isStart;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountHolder() {
		return accountHolder;
	}

	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getIdCardPicPath() {
		return idCardPicPath;
	}

	public void setIdCardPicPath(String idCardPicPath) {
		this.idCardPicPath = idCardPicPath;
	}

	public String getBankPicPath() {
		return bankPicPath;
	}

	public void setBankPicPath(String bankPicPath) {
		this.bankPicPath = bankPicPath;
	}

	@Override
	public String toString() {
		return "User{" +
				"id=" + id +
				", loginCode='" + loginCode + '\'' +
				", password='" + password + '\'' +
				", salt='" + salt + '\'' +
				", userName='" + userName + '\'' +
				", sex='" + sex + '\'' +
				", birthday=" + birthday +
				", cardType='" + cardType + '\'' +
				", cardTypeName='" + cardTypeName + '\'' +
				", idCard='" + idCard + '\'' +
				", country='" + country + '\'' +
				", mobile='" + mobile + '\'' +
				", email='" + email + '\'' +
				", userAddress='" + userAddress + '\'' +
				", postCode='" + postCode + '\'' +
				", createTime=" + createTime +
				", userType='" + userType + '\'' +
				", userTypeName='" + userTypeName + '\'' +
				", isStart=" + isStart +
				", lastUpdateTime=" + lastUpdateTime +
				", lastLoginTime=" + lastLoginTime +
				", bankName='" + bankName + '\'' +
				", accountHolder='" + accountHolder + '\'' +
				", bankAccount='" + bankAccount + '\'' +
				", idCardPicPath='" + idCardPicPath + '\'' +
				", bankPicPath='" + bankPicPath + '\'' +
				", roleList=" + roleList +
				'}';
	}
}
