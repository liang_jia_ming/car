package com.car.car.pojo;

import java.io.Serializable;

public class MarketListPage implements Serializable {
    private int id;
    private String cartype;
    private String sortname;
    private double jinjia;
    private double selling_price;
    private double total;
    private String userName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCartype() {
        return cartype;
    }

    public void setCartype(String cartype) {
        this.cartype = cartype;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public double getJinjia() {
        return jinjia;
    }

    public void setJinjia(double jinjia) {
        this.jinjia = jinjia;
    }

    public double getSelling_price() {
        return selling_price;
    }

    public void setSelling_price(double selling_price) {
        this.selling_price = selling_price;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "MarketListPage{" +
                "id=" + id +
                ", cartype='" + cartype + '\'' +
                ", sortname='" + sortname + '\'' +
                ", jinjia=" + jinjia +
                ", selling_price=" + selling_price +
                ", total=" + total +
                ", userName='" + userName + '\'' +
                '}';
    }
}
