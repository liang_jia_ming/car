package com.car.car.pojo;


public class Carbrand {

  private int  sortid;
  private String sortname;


  public int  getSortid() {
    return sortid;
  }

  public void setSortid(int  sortid) {
    this.sortid = sortid;
  }


  public String getSortname() {
    return sortname;
  }

  public void setSortname(String sortname) {
    this.sortname = sortname;
  }

  @Override
  public String toString() {
    return "Carbrand{" +
            "sortid=" + sortid +
            ", sortname='" + sortname + '\'' +
            '}';
  }
}
