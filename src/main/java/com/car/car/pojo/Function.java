package com.car.car.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Function
 * @author bdqn_hl
 * @date 2014-2-21
 */
public class Function implements Serializable {
	
	private int id;
	private String functionCode;
	private String functionName;
	private String funcUrl;
	private int parentId;
	private int level ;
	private Date creationTime;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFunctionCode() {
		return functionCode;
	}
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFuncUrl() {
		return funcUrl;
	}
	public void setFuncUrl(String funcUrl) {
		this.funcUrl = funcUrl;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public Date getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "Function{" +
				"id=" + id +
				", functionCode='" + functionCode + '\'' +
				", functionName='" + functionName + '\'' +
				", funcUrl='" + funcUrl + '\'' +
				", parentId=" + parentId +
				", level=" + level +
				", creationTime=" + creationTime +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Function function = (Function) o;
		return Objects.equals(id, function.id);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id);
	}
}
