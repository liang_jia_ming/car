package com.car.car.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Role implements Serializable {
	
	private int id;
	private String roleCode;
	private String roleName;
	private int isStart;
	private Date createDate;
	private String createdBy;

	private List<Function> functionList ; //角色对功能 1：n

	public List<Function> getFunctionList() {
		return functionList;
	}

	public void setFunctionList(List<Function> functionList) {
		this.functionList = functionList;
	}

	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public int getIsStart() {
		return isStart;
	}
	public void setIsStart(int isStart) {
		this.isStart = isStart;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", roleCode='" + roleCode + '\'' +
				", roleName='" + roleName + '\'' +
				", isStart=" + isStart +
				", createDate=" + createDate +
				", createdBy='" + createdBy + '\'' +
				", functionList=" + functionList +
				'}';
	}
}
