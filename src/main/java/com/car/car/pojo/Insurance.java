package com.car.car.pojo;

import java.util.Date;

public class Insurance {

  private int inid;
  private String insuranceCompany;
  private String insurancename;
  private double insurancePrice;
  private Date insuranceDate;
  private Date insuranceExpire;
  private String insuranceNote;


  public int getInid() {
    return inid;
  }

  public void setInid(int inid) {
    this.inid = inid;
  }


  public String getInsuranceCompany() {
    return insuranceCompany;
  }

  public void setInsuranceCompany(String insuranceCompany) {
    this.insuranceCompany = insuranceCompany;
  }


  public String getInsurancename() {
    return insurancename;
  }

  public void setInsurancename(String insurancename) {
    this.insurancename = insurancename;
  }


  public double getInsurancePrice() {
    return insurancePrice;
  }

  public void setInsurancePrice(double insurancePrice) {
    this.insurancePrice = insurancePrice;
  }


  public Date getInsuranceDate() {
    return insuranceDate;
  }

  public void setInsuranceDate(java.sql.Date insuranceDate) {
    this.insuranceDate = insuranceDate;
  }


  public Date getInsuranceExpire() {
    return insuranceExpire;
  }

  public void setInsuranceExpire(java.sql.Date insuranceExpire) {
    this.insuranceExpire = insuranceExpire;
  }


  public String getInsuranceNote() {
    return insuranceNote;
  }

  public void setInsuranceNote(String insuranceNote) {
    this.insuranceNote = insuranceNote;
  }

  @Override
  public String toString() {
    return "Insurance{" +
            "inid=" + inid +
            ", insuranceCompany='" + insuranceCompany + '\'' +
            ", insurancename='" + insurancename + '\'' +
            ", insurancePrice=" + insurancePrice +
            ", insuranceDate=" + insuranceDate +
            ", insuranceExpire=" + insuranceExpire +
            ", insuranceNote='" + insuranceNote + '\'' +
            '}';
  }
}
