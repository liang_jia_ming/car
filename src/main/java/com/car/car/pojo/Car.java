package com.car.car.pojo;

import java.util.Date;

public class Car {

  private int  carid;
  private String img;
  private String carcode;
  private String cartype;
  private int  sortid;
  private int  seriesid;
  private double carprice;
  private String carperformance;
  private String carcolour;
  private int  sid;
  private String carnumber;
  private String engineno;
  private Date factorydate;
  private Date storagedate;
  private String note;


  public int  getCarid() {
    return carid;
  }

  public void setCarid(int  carid) {
    this.carid = carid;
  }


  public String getImg() {
    return img;
  }

  public void setImg(String img) {
    this.img = img;
  }


  public String getCarcode() {
    return carcode;
  }

  public void setCarcode(String carcode) {
    this.carcode = carcode;
  }


  public String getCartype() {
    return cartype;
  }

  public void setCartype(String cartype) {
    this.cartype = cartype;
  }


  public int  getSortid() {
    return sortid;
  }

  public void setSortid(int  sortid) {
    this.sortid = sortid;
  }


  public int  getSeriesid() {
    return seriesid;
  }

  public void setSeriesid(int  seriesid) {
    this.seriesid = seriesid;
  }


  public double getCarprice() {
    return carprice;
  }

  public void setCarprice(double carprice) {
    this.carprice = carprice;
  }


  public String getCarperformance() {
    return carperformance;
  }

  public void setCarperformance(String carperformance) {
    this.carperformance = carperformance;
  }


  public String getCarcolour() {
    return carcolour;
  }

  public void setCarcolour(String carcolour) {
    this.carcolour = carcolour;
  }


  public int  getSid() {
    return sid;
  }

  public void setSid(int  sid) {
    this.sid = sid;
  }


  public String getCarnumber() {
    return carnumber;
  }

  public void setCarnumber(String carnumber) {
    this.carnumber = carnumber;
  }


  public String getEngineno() {
    return engineno;
  }

  public void setEngineno(String engineno) {
    this.engineno = engineno;
  }


  public Date getFactorydate() {
    return factorydate;
  }

  public void setFactorydate(java.sql.Date factorydate) {
    this.factorydate = factorydate;
  }


  public Date getStoragedate() {
    return storagedate;
  }

  public void setStoragedate(java.sql.Date storagedate) {
    this.storagedate = storagedate;
  }


  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note;
  }

  @Override
  public String toString() {
    return "Car{" +
            "carid=" + carid +
            ", img='" + img + '\'' +
            ", carcode='" + carcode + '\'' +
            ", cartype='" + cartype + '\'' +
            ", sortid=" + sortid +
            ", seriesid=" + seriesid +
            ", carprice=" + carprice +
            ", carperformance='" + carperformance + '\'' +
            ", carcolour='" + carcolour + '\'' +
            ", sid=" + sid +
            ", carnumber='" + carnumber + '\'' +
            ", engineno='" + engineno + '\'' +
            ", factorydate=" + factorydate +
            ", storagedate=" + storagedate +
            ", note='" + note + '\'' +
            '}';
  }
}
