package com.car.car.pojo;

public class TreeNode {
        private String id;
        private String pid;
        private String name;
        private String open;
        private String isParent;
        private boolean checked = false ;

        public TreeNode(){}

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getPid() {
            return pid;
        }
        public void setPid(String pid) {
            this.pid = pid;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getOpen() {
            return open;
        }
        public void setOpen(String open) {
            this.open = open;
        }
        public String getIsParent() {
            return isParent;
        }
        public void setIsParent(String isParent) {
            this.isParent = isParent;
        }

    @Override
    public String toString() {
        return "TreeNode{" +
                "id='" + id + '\'' +
                ", pid='" + pid + '\'' +
                ", name='" + name + '\'' +
                ", open='" + open + '\'' +
                ", isParent='" + isParent + '\'' +
                ", checked=" + checked +
                '}';
    }
}
