package com.car.car.pojo;


public class Custtype {

  private int custid;
  private String custname;


  public int getCustid() {
    return custid;
  }

  public void setCustid(int custid) {
    this.custid = custid;
  }


  public String getCustname() {
    return custname;
  }

  public void setCustname(String custname) {
    this.custname = custname;
  }

  @Override
  public String toString() {
    return "Custtype{" +
            "custid=" + custid +
            ", custname='" + custname + '\'' +
            '}';
  }
}
