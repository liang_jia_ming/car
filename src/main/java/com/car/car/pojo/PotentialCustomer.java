package com.car.car.pojo;

import java.util.Date;

public class PotentialCustomer {

  private long pid;
  private String pName;
  private String phone;
  private long pSex;
  private String pDuty;
  private String pAddress;
  private String testDrive;
  private double budget;
  private String nowCar;
  private String km;
  private long followRid;
  private String cause;
  private double prepay;
  private Date pData;
  private String impress;


  public long getPid() {
    return pid;
  }

  public void setPid(long pid) {
    this.pid = pid;
  }


  public String getPName() {
    return pName;
  }

  public void setPName(String pName) {
    this.pName = pName;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public long getPSex() {
    return pSex;
  }

  public void setPSex(long pSex) {
    this.pSex = pSex;
  }


  public String getPDuty() {
    return pDuty;
  }

  public void setPDuty(String pDuty) {
    this.pDuty = pDuty;
  }


  public String getPAddress() {
    return pAddress;
  }

  public void setPAddress(String pAddress) {
    this.pAddress = pAddress;
  }


  public String getTestDrive() {
    return testDrive;
  }

  public void setTestDrive(String testDrive) {
    this.testDrive = testDrive;
  }


  public double getBudget() {
    return budget;
  }

  public void setBudget(double budget) {
    this.budget = budget;
  }


  public String getNowCar() {
    return nowCar;
  }

  public void setNowCar(String nowCar) {
    this.nowCar = nowCar;
  }


  public String getKm() {
    return km;
  }

  public void setKm(String km) {
    this.km = km;
  }


  public long getFollowRid() {
    return followRid;
  }

  public void setFollowRid(long followRid) {
    this.followRid = followRid;
  }


  public String getCause() {
    return cause;
  }

  public void setCause(String cause) {
    this.cause = cause;
  }


  public double getPrepay() {
    return prepay;
  }

  public void setPrepay(double prepay) {
    this.prepay = prepay;
  }


  public Date getPData() {
    return pData;
  }

  public void setPData(java.sql.Date pData) {
    this.pData = pData;
  }


  public String getImpress() {
    return impress;
  }

  public void setImpress(String impress) {
    this.impress = impress;
  }

  @Override
  public String toString() {
    return "PotentialCustomer{" +
            "pid=" + pid +
            ", pName='" + pName + '\'' +
            ", phone='" + phone + '\'' +
            ", pSex=" + pSex +
            ", pDuty='" + pDuty + '\'' +
            ", pAddress='" + pAddress + '\'' +
            ", testDrive='" + testDrive + '\'' +
            ", budget=" + budget +
            ", nowCar='" + nowCar + '\'' +
            ", km='" + km + '\'' +
            ", followRid=" + followRid +
            ", cause='" + cause + '\'' +
            ", prepay=" + prepay +
            ", pData=" + pData +
            ", impress='" + impress + '\'' +
            '}';
  }
}
