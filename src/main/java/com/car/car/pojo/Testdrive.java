package com.car.car.pojo;


public class Testdrive {

  private int testid;
  private int customerid;
  private int sortid;
  private int carid;
  private String result;
  private int state;


  public int getTestid() {
    return testid;
  }

  public void setTestid(int testid) {
    this.testid = testid;
  }


  public int getCustomerid() {
    return customerid;
  }

  public void setCustomerid(int customerid) {
    this.customerid = customerid;
  }


  public int getSortid() {
    return sortid;
  }

  public void setSortid(int sortid) {
    this.sortid = sortid;
  }


  public int getCarid() {
    return carid;
  }

  public void setCarid(int carid) {
    this.carid = carid;
  }


  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }


  public int getState() {
    return state;
  }

  public void setState(int state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "Testdrive{" +
            "testid=" + testid +
            ", customerid=" + customerid +
            ", sortid=" + sortid +
            ", carid=" + carid +
            ", result='" + result + '\'' +
            ", state=" + state +
            '}';
  }
}
