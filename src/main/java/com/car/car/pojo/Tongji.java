package com.car.car.pojo;


public class Tongji {

  private int tid;
  private int cbid;
  private int customerid;
  private String carnumber;


  public int getTid() {
    return tid;
  }

  public void setTid(int tid) {
    this.tid = tid;
  }


  public int getCbid() {
    return cbid;
  }

  public void setCbid(int cbid) {
    this.cbid = cbid;
  }


  public int getCustomerid() {
    return customerid;
  }

  public void setCustomerid(int customerid) {
    this.customerid = customerid;
  }


  public String getCarnumber() {
    return carnumber;
  }

  public void setCarnumber(String carnumber) {
    this.carnumber = carnumber;
  }

  @Override
  public String toString() {
    return "Tongji{" +
            "tid=" + tid +
            ", cbid=" + cbid +
            ", customerid=" + customerid +
            ", carnumber='" + carnumber + '\'' +
            '}';
  }
}
