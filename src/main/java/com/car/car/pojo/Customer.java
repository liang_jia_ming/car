package com.car.car.pojo;

import java.util.Date;

public class Customer {

  private int  customerid;
  private String customername;
  private int  idcard;
  private int  sex;
  private String address;
  private String customertel;
  private String duty;
  private int  marketOdd;
  private int  rid;
  private String caruser;
  private String carcolour;
  private int  engineno;
  private int  qualifiedNumber;
  private String insurancename;
  private double insurancePrice;
  private Date insuranceDate;
  private Date insuranceExpire;
  private java.sql.Date returnDate;
  private String platenumber;
  private int  sortid;


  public int  getCustomerid() {
    return customerid;
  }

  public void setCustomerid(int  customerid) {
    this.customerid = customerid;
  }


  public String getCustomername() {
    return customername;
  }

  public void setCustomername(String customername) {
    this.customername = customername;
  }


  public int  getIdcard() {
    return idcard;
  }

  public void setIdcard(int  idcard) {
    this.idcard = idcard;
  }


  public int  getSex() {
    return sex;
  }

  public void setSex(int  sex) {
    this.sex = sex;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getCustomertel() {
    return customertel;
  }

  public void setCustomertel(String customertel) {
    this.customertel = customertel;
  }


  public String getDuty() {
    return duty;
  }

  public void setDuty(String duty) {
    this.duty = duty;
  }


  public int  getMarketOdd() {
    return marketOdd;
  }

  public void setMarketOdd(int  marketOdd) {
    this.marketOdd = marketOdd;
  }


  public int  getRid() {
    return rid;
  }

  public void setRid(int  rid) {
    this.rid = rid;
  }


  public String getCaruser() {
    return caruser;
  }

  public void setCaruser(String caruser) {
    this.caruser = caruser;
  }


  public String getCarcolour() {
    return carcolour;
  }

  public void setCarcolour(String carcolour) {
    this.carcolour = carcolour;
  }


  public int  getEngineno() {
    return engineno;
  }

  public void setEngineno(int  engineno) {
    this.engineno = engineno;
  }


  public int  getQualifiedNumber() {
    return qualifiedNumber;
  }

  public void setQualifiedNumber(int  qualifiedNumber) {
    this.qualifiedNumber = qualifiedNumber;
  }


  public String getInsurancename() {
    return insurancename;
  }

  public void setInsurancename(String insurancename) {
    this.insurancename = insurancename;
  }


  public double getInsurancePrice() {
    return insurancePrice;
  }

  public void setInsurancePrice(double insurancePrice) {
    this.insurancePrice = insurancePrice;
  }


  public Date getInsuranceDate() {
    return insuranceDate;
  }

  public void setInsuranceDate(java.sql.Date insuranceDate) {
    this.insuranceDate = insuranceDate;
  }


  public Date getInsuranceExpire() {
    return insuranceExpire;
  }

  public void setInsuranceExpire(java.sql.Date insuranceExpire) {
    this.insuranceExpire = insuranceExpire;
  }


  public java.sql.Date getReturnDate() {
    return returnDate;
  }

  public void setReturnDate(java.sql.Date returnDate) {
    this.returnDate = returnDate;
  }


  public String getPlatenumber() {
    return platenumber;
  }

  public void setPlatenumber(String platenumber) {
    this.platenumber = platenumber;
  }


  public int  getSortid() {
    return sortid;
  }

  public void setSortid(int  sortid) {
    this.sortid = sortid;
  }

  @Override
  public String toString() {
    return "Customer{" +
            "customerid=" + customerid +
            ", customername='" + customername + '\'' +
            ", idcard=" + idcard +
            ", sex=" + sex +
            ", address='" + address + '\'' +
            ", customertel='" + customertel + '\'' +
            ", duty='" + duty + '\'' +
            ", marketOdd=" + marketOdd +
            ", rid=" + rid +
            ", caruser='" + caruser + '\'' +
            ", carcolour='" + carcolour + '\'' +
            ", engineno=" + engineno +
            ", qualifiedNumber=" + qualifiedNumber +
            ", insurancename='" + insurancename + '\'' +
            ", insurancePrice=" + insurancePrice +
            ", insuranceDate=" + insuranceDate +
            ", insuranceExpire=" + insuranceExpire +
            ", returnDate=" + returnDate +
            ", platenumber='" + platenumber + '\'' +
            ", sortid=" + sortid +
            '}';
  }
}
