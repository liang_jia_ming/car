package com.car.car.pojo;

/**
 * 角色对应权限数量统计
 */
public class RoleFunctionStatistic {

    private String roleName ;//角色名称
    private int funcCount ; //功能数量

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getFuncCount() {
        return funcCount;
    }

    public void setFuncCount(int funcCount) {
        this.funcCount = funcCount;
    }
}
