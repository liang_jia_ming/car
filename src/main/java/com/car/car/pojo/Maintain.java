package com.car.car.pojo;

import java.util.Date;

public class Maintain {

  private int mid;
  private int customerid;
  private int carid;
  private int inid;
  private String customername;
  private String idcard;
  private int sex;
  private String address;
  private String customertel;
  private String cartype;
  private String place;
  private String insurancename;
  private double insurancePrice;
  private Date mData;


  public int getMid() {
    return mid;
  }

  public void setMid(int mid) {
    this.mid = mid;
  }


  public int getCustomerid() {
    return customerid;
  }

  public void setCustomerid(int customerid) {
    this.customerid = customerid;
  }


  public int getCarid() {
    return carid;
  }

  public void setCarid(int carid) {
    this.carid = carid;
  }


  public int getInid() {
    return inid;
  }

  public void setInid(int inid) {
    this.inid = inid;
  }


  public String getCustomername() {
    return customername;
  }

  public void setCustomername(String customername) {
    this.customername = customername;
  }


  public String getIdcard() {
    return idcard;
  }

  public void setIdcard(String idcard) {
    this.idcard = idcard;
  }


  public int getSex() {
    return sex;
  }

  public void setSex(int sex) {
    this.sex = sex;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getCustomertel() {
    return customertel;
  }

  public void setCustomertel(String customertel) {
    this.customertel = customertel;
  }


  public String getCartype() {
    return cartype;
  }

  public void setCartype(String cartype) {
    this.cartype = cartype;
  }


  public String getPlace() {
    return place;
  }

  public void setPlace(String place) {
    this.place = place;
  }


  public String getInsurancename() {
    return insurancename;
  }

  public void setInsurancename(String insurancename) {
    this.insurancename = insurancename;
  }


  public double getInsurancePrice() {
    return insurancePrice;
  }

  public void setInsurancePrice(double insurancePrice) {
    this.insurancePrice = insurancePrice;
  }


  public Date getMData() {
    return mData;
  }

  public void setMData(java.sql.Date mData) {
    this.mData = mData;
  }

  @Override
  public String toString() {
    return "Maintain{" +
            "mid=" + mid +
            ", customerid=" + customerid +
            ", carid=" + carid +
            ", inid=" + inid +
            ", customername='" + customername + '\'' +
            ", idcard='" + idcard + '\'' +
            ", sex=" + sex +
            ", address='" + address + '\'' +
            ", customertel='" + customertel + '\'' +
            ", cartype='" + cartype + '\'' +
            ", place='" + place + '\'' +
            ", insurancename='" + insurancename + '\'' +
            ", insurancePrice=" + insurancePrice +
            ", mData=" + mData +
            '}';
  }
}
