package com.car.car.pojo;


public class Carseries {

  private int  seriesid;
  private String seriesname;


  public int  getSeriesid() {
    return seriesid;
  }

  public void setSeriesid(int  seriesid) {
    this.seriesid = seriesid;
  }


  public String getSeriesname() {
    return seriesname;
  }

  public void setSeriesname(String seriesname) {
    this.seriesname = seriesname;
  }

}
