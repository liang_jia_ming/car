package com.car.car.pojo;

import java.io.Serializable;

public class CarListPage implements Serializable {
    private String img;
    private int carid;
    private String cartype;
    private String sortname;
    private String seriesname;
    private double carprice;
    private String carperformance;
    private String carcolour;
    private String sname;
    private String carnumber;
    private String describe;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public double getCarprice() {
        return carprice;
    }

    public void setCarprice(double carprice) {
        this.carprice = carprice;
    }

    public String getCarperformance() {
        return carperformance;
    }

    public void setCarperformance(String carperformance) {
        this.carperformance = carperformance;
    }

    public String getCarcolour() {
        return carcolour;
    }

    public void setCarcolour(String carcolour) {
        this.carcolour = carcolour;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public void setCarnumber(String carnumber) {
        this.carnumber = carnumber;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public String getSeriesname() {
        return seriesname;
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public void setCartype(String cartype) {
        this.cartype = cartype;
    }


    public int getCarid() {
        return carid;
    }

    public String getCartype() {
        return cartype;
    }

    @Override
    public String toString() {
        return "CarListPage{" +
                "img='" + img + '\'' +
                ", carid=" + carid +
                ", cartype='" + cartype + '\'' +
                ", sortname='" + sortname + '\'' +
                ", seriesname='" + seriesname + '\'' +
                ", carprice=" + carprice +
                ", carperformance='" + carperformance + '\'' +
                ", carcolour='" + carcolour + '\'' +
                ", sname='" + sname + '\'' +
                ", carnumber='" + carnumber + '\'' +
                ", describe='" + describe + '\'' +
                '}';
    }
}
