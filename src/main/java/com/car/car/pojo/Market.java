package com.car.car.pojo;


public class Market {

  private int id;
  private int rid;
  private java.sql.Date marketDate;
  private int marketOdd;
  private int custid;
  private String customername;
  private String customertel;
  private int idcard;
  private String duty;
  private String cartype;
  private String carcode;
  private String carcolour;
  private String engineno;
  private String gift;
  private String insurancename;
  private double insuranceFord;
  private double carprice;
  private double selling;
  private double sale;
  private double handle;
  private String comment;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }


  public int getRid() {
    return rid;
  }

  public void setRid(int rid) {
    this.rid = rid;
  }


  public java.sql.Date getMarketDate() {
    return marketDate;
  }

  public void setMarketDate(java.sql.Date marketDate) {
    this.marketDate = marketDate;
  }


  public int getMarketOdd() {
    return marketOdd;
  }

  public void setMarketOdd(int marketOdd) {
    this.marketOdd = marketOdd;
  }


  public int getCustid() {
    return custid;
  }

  public void setCustid(int custid) {
    this.custid = custid;
  }


  public String getCustomername() {
    return customername;
  }

  public void setCustomername(String customername) {
    this.customername = customername;
  }


  public String getCustomertel() {
    return customertel;
  }

  public void setCustomertel(String customertel) {
    this.customertel = customertel;
  }


  public int getIdcard() {
    return idcard;
  }

  public void setIdcard(int idcard) {
    this.idcard = idcard;
  }


  public String getDuty() {
    return duty;
  }

  public void setDuty(String duty) {
    this.duty = duty;
  }


  public String getCartype() {
    return cartype;
  }

  public void setCartype(String cartype) {
    this.cartype = cartype;
  }


  public String getCarcode() {
    return carcode;
  }

  public void setCarcode(String carcode) {
    this.carcode = carcode;
  }


  public String getCarcolour() {
    return carcolour;
  }

  public void setCarcolour(String carcolour) {
    this.carcolour = carcolour;
  }


  public String getEngineno() {
    return engineno;
  }

  public void setEngineno(String engineno) {
    this.engineno = engineno;
  }


  public String getGift() {
    return gift;
  }

  public void setGift(String gift) {
    this.gift = gift;
  }


  public String getInsurancename() {
    return insurancename;
  }

  public void setInsurancename(String insurancename) {
    this.insurancename = insurancename;
  }


  public double getInsuranceFord() {
    return insuranceFord;
  }

  public void setInsuranceFord(double insuranceFord) {
    this.insuranceFord = insuranceFord;
  }


  public double getCarprice() {
    return carprice;
  }

  public void setCarprice(double carprice) {
    this.carprice = carprice;
  }


  public double getSelling() {
    return selling;
  }

  public void setSelling(double selling) {
    this.selling = selling;
  }


  public double getSale() {
    return sale;
  }

  public void setSale(double sale) {
    this.sale = sale;
  }


  public double getHandle() {
    return handle;
  }

  public void setHandle(double handle) {
    this.handle = handle;
  }


  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Override
  public String toString() {
    return "Market{" +
            "id=" + id +
            ", rid=" + rid +
            ", marketDate=" + marketDate +
            ", marketOdd=" + marketOdd +
            ", custid=" + custid +
            ", customername='" + customername + '\'' +
            ", customertel='" + customertel + '\'' +
            ", idcard=" + idcard +
            ", duty='" + duty + '\'' +
            ", cartype='" + cartype + '\'' +
            ", carcode='" + carcode + '\'' +
            ", carcolour='" + carcolour + '\'' +
            ", engineno='" + engineno + '\'' +
            ", gift='" + gift + '\'' +
            ", insurancename='" + insurancename + '\'' +
            ", insuranceFord=" + insuranceFord +
            ", carprice=" + carprice +
            ", selling=" + selling +
            ", sale=" + sale +
            ", handle=" + handle +
            ", comment='" + comment + '\'' +
            '}';
  }
}
