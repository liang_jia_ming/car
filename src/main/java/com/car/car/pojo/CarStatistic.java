package com.car.car.pojo;

/**
 * 品牌对应汽车数量统计
 */
public class CarStatistic {

    private String sortname ;//品牌名称
    private int funcCount  ; //功能数量

    public int getFuncCount() {
        return funcCount;
    }

    public void setFuncCount(int funcCount) {
        this.funcCount = funcCount;
    }


    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    @Override
    public String toString() {
        return "CarStatistic{" +
                "sortname='" + sortname + '\'' +
                ", funcCount=" + funcCount +
                '}';
    }
}
