package com.car.car.pojo;

import java.io.Serializable;

public class CustomerListPage implements Serializable {
    private int customerid;
    private String customername;
    private String customertel;
    private String platenumbe;
    private int carid;
    private String cartype;
    private String carcolour;
    private int sortid;
    private String sortname;
    private int id;
    private String insurancename;
    private String weixiu;
    private String use_insurance;

    public int getCustomerid() {
        return customerid;
    }

    public void setCustomerid(int customerid) {
        this.customerid = customerid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomertel() {
        return customertel;
    }

    public void setCustomertel(String customertel) {
        this.customertel = customertel;
    }

    public String getPlatenumbe() {
        return platenumbe;
    }

    public void setPlatenumbe(String platenumbe) {
        this.platenumbe = platenumbe;
    }

    public int getCarid() {
        return carid;
    }

    public void setCarid(int carid) {
        this.carid = carid;
    }

    public String getCartype() {
        return cartype;
    }

    public void setCartype(String cartype) {
        this.cartype = cartype;
    }

    public String getCarcolour() {
        return carcolour;
    }

    public void setCarcolour(String carcolour) {
        this.carcolour = carcolour;
    }

    public int getSortid() {
        return sortid;
    }

    public void setSortid(int sortid) {
        this.sortid = sortid;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInsurancename() {
        return insurancename;
    }

    public void setInsurancename(String insurancename) {
        this.insurancename = insurancename;
    }

    public String getWeixiu() {
        return weixiu;
    }

    public void setWeixiu(String weixiu) {
        this.weixiu = weixiu;
    }

    public String getUse_insurance() {
        return use_insurance;
    }

    public void setUse_insurance(String use_insurance) {
        this.use_insurance = use_insurance;
    }

    @Override
    public String toString() {
        return "CustomerListPage{" +
                "customerid=" + customerid +
                ", customername='" + customername + '\'' +
                ", customertel='" + customertel + '\'' +
                ", platenumbe='" + platenumbe + '\'' +
                ", carid=" + carid +
                ", cartype='" + cartype + '\'' +
                ", carcolour='" + carcolour + '\'' +
                ", sortid=" + sortid +
                ", sortname='" + sortname + '\'' +
                ", id=" + id +
                ", insurancename='" + insurancename + '\'' +
                ", weixiu='" + weixiu + '\'' +
                ", use_insurance='" + use_insurance + '\'' +
                '}';
    }
}
