package com.car.car.dao.function;

import com.car.car.pojo.Function;

import java.util.List;

public interface FunctionDao {

    //返回一级功能列表
    List<Function> getFirstFunctionList() ;

    //根据父级id返回下级功能列表
    List<Function> getChildrenFunctionList(int parentId) ;

    //根据功能id获取功能信息
    Function getFunctionInfo(int id) ;

    //根据用户id获取对应的功能权限列表
    List<Function> getFunctionListByUser(int uid) ;

    //根据角色id获取对应的功能权限列表
    List<Function> getFunctionListByRole(int rid) ;

    //返回一级菜单列表
    List<Function> getFirstMenuList() ;

    //根据父级id返回二级菜单列表
    List<Function> getSecondMenuList(int parentId) ;



    //添加功能
    int addFunction(Function function) ;

    //修改功能
    int updateFunction(Function function) ;


    //删除功能（功能轻易不会被删除，一般用状态代替是否开启该功能）
    int deleteFunction(int fid) ;
}
