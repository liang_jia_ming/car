package com.car.car.dao.user;

import com.car.car.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    //根据loginCode获取用户信息
    User getUserByLoginCode(String loginCode) ;

    //添加用户
    int addUser(User user) ;

    //用户列表
    List<User> getAllUsers() ;

    //根据用户id获取用户信息
    User getUserById(int uid) ;

    //根据用户选择的角色添加到中间表（添加或修改用户时，关联添加中间表）
    int addUserRole(@Param("uid") int uid, @Param("rid") int rid) ;

    //根据用户id删除，删除用户角色中间表（修改用户时的关联动作）
    int deleteUserRoleByUser(int uid) ;


    //根据角色删除用户角色中间表(在删除角色时，关联删除)
    int deleteUserRoleByRole(int rid) ;
}
