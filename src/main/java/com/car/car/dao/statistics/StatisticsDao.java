package com.car.car.dao.statistics;

import com.car.car.pojo.RoleFunctionStatistic;

import java.util.List;

public interface StatisticsDao {
    //统计角色拥有的权限数量
    List<RoleFunctionStatistic> getRoleFunctionCount() ;
}
