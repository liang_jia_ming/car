package com.car.car.dao.authority;

import com.car.car.pojo.Authority;

/**
 * 授权dao接口
 */
public interface AuthorityDao {

    //根据角色添加授权
    int addAuthority(Authority authority) ;


    //根据角色id删除权限
    int deleteAuthorityByRole(int rid) ;




}
