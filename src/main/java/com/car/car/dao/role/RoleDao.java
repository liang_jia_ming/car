package com.car.car.dao.role;

import com.car.car.pojo.Role;

import java.util.List;

public interface RoleDao {

    //角色列表
    List<Role> getAllRoles() ;

    //根据角色id返回角色信息
    Role getRoleInfoById(int rid) ;



    //添加角色
    int addRole(Role role) ;


    //修改角色
    int updateRole(Role role) ;


    //删除角色
    int deleteRole(int rid) ;
}
