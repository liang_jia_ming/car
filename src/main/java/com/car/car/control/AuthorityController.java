package com.car.car.control;

import com.car.car.pojo.Function;
import com.car.car.pojo.Role;
import com.car.car.pojo.TreeNode;
import com.car.car.service.authority.AuthorityService;
import com.car.car.service.function.FunctionService;
import com.car.car.service.role.RoleService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/authority")
public class AuthorityController {

    @Resource
    private RoleService roleService ;

    @Resource
    private FunctionService functionService ;

    @Resource
    private AuthorityService authorityService ;

    @RequestMapping("/authorityManage")
    @RequiresPermissions("authorityManage")
    public String authorityManage(Model model) {
        List<Role> roleList = roleService.getAllRoles() ;
        model.addAttribute("roleList", roleList) ;

        return "authority/authorityManage" ;
    }

    @RequestMapping("/getRoleFunctions")
    @RequiresPermissions("authorityManage")
    @ResponseBody
    public List<TreeNode> getRoleFunctions(@RequestParam(value = "rid", defaultValue = "0") int rid) {

        //先获取角色下有哪些功能
        List<Function> roleFunctions = functionService.getFunctionListByRole(rid) ;

        //获取所有功能列表
        List<Function> functionList = new ArrayList<>() ;
        functionService.getAllFunctions(functionList, 0) ;

        //循环判断角色对应的功能列表，对应的上的则变成选中状态
        List<TreeNode> treeNodes = new ArrayList<>() ;

        for(Function function : functionList) {
            TreeNode treeNode = new TreeNode() ;
            treeNode.setId(function.getId()+"");
            treeNode.setName(function.getFunctionName());
            treeNode.setPid(function.getParentId()+"");
            treeNode.setIsParent("true");
            treeNode.setOpen("true");

            if(roleFunctions.contains(function)) {
                treeNode.setChecked(true);
            }

            treeNodes.add(treeNode) ;
        }

        return treeNodes ;
    }

    @RequestMapping("/saveRoleFunctions")
    @RequiresPermissions("authorityManage")
    @ResponseBody
    public int saveRoleFunctions(int rid, String functionIds) {
        int success = authorityService.addAuthority(rid, functionIds) ;
        return success ;
    }
}
