package com.car.car.control;

import com.car.car.pojo.Function;
import com.car.car.pojo.Menu;
import com.car.car.service.menu.MenuService;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {
    @Autowired
    private MenuService menuService ;

    @RequestMapping("/login")
    public String login(HttpServletRequest request, Map<String, Object> map) throws Exception{
        System.out.println("HomeController.login()");
        // 登录失败从request中获取shiro处理的异常信息。
        // shiroLoginFailure:就是shiro异常类的全类名.
        String exception = (String) request.getAttribute("shiroLoginFailure");
        System.out.println("exception=" + exception);
        String msg = "";
        if (exception != null) {
            if (UnknownAccountException.class.getName().equals(exception)) {
                System.out.println("UnknownAccountException -- > 账号不存在：");
                msg = "UnknownAccountException -- > 账号不存在：";
            } else if (IncorrectCredentialsException.class.getName().equals(exception)) {
                System.out.println("IncorrectCredentialsException -- > 密码不正确：");
                msg = "IncorrectCredentialsException -- > 密码不正确：";
            }
        }
        map.put("msg", msg);
//        // 此方法不处理登录成功,由shiro进行处理
        return "login";
    }


    @RequestMapping({"/index","/",""})
    public String index(Model model) {
        List<Menu> menuList = menuService.getMenuList() ;
        for(Menu ss:menuList){
            System.out.println(ss.getTopFunc().getFunctionName());
            for(Function sf:ss.getChildrenFunc()){
                System.out.println(sf.getFunctionName());
            }
        }
        System.out.println(menuList.get(0).getTopFunc());
        model.addAttribute("menuList", menuList) ;
        return "index";
    }
}
