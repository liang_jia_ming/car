package com.car.car.control;

import com.car.car.pojo.Role;
import com.car.car.pojo.User;
import com.car.car.service.role.RoleService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @RequestMapping("/roleList")
    @RequiresPermissions("roleList")
    public String roleList(Model model) {

        List<Role> roleList = roleService.getAllRoles();
        model.addAttribute("roleList",roleList) ;

        return "role/roleList" ;
    }

    @RequestMapping("/roleAdd")
    @RequiresPermissions("roleAdd")
    public String getRole(){
        return "role/roleAdd";
    }
    //
    @PostMapping("/doRoleAdd")
    @RequiresPermissions("roleAdd")
    public String doRoleAdd(Role role, Model model){
        User userInfo = (User) SecurityUtils.getSubject().getSession().getAttribute("currentUser");
        System.out.println(userInfo.getId());
        int num = roleService.addRole(role);
        String msg = "";
        if(num == 1){
            msg = "添加角色成功" ;
        }else {
            msg = "添加角色失败" ;
        }
        model.addAttribute("msg",msg);
        return "forward:/role/roleList";
    }


    @GetMapping("/roleDel/{id}")
    @RequiresPermissions("roleDel")

    public String delCar(@PathVariable("id") int id, Model model){
        int num = 0;
        String msg ="";
        num = roleService.deleteRole(id);
        if(num == 1){
            msg = "删除汽车成功";
        }else{
            msg = "删除汽车成功";
        }
        return "forward:/role/roleList";
    }
}

