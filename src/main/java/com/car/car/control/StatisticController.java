package com.car.car.control;

import com.car.car.service.statistics.StatisticsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@Controller
@RequestMapping("/statistic")
public class StatisticController {

    @Resource
    private StatisticsService statisticsService ;

    //点击品牌统计跳到页面显示
    @RequestMapping("/carCountSatistic")
    @RequiresPermissions("car_count_statistic")
    public String car_count_statistic() {
        return "statistic/carCountSatistic";
    }

}
