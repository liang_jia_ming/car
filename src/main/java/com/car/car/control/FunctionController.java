package com.car.car.control;

import com.car.car.pojo.Function;
import com.car.car.pojo.TreeNode;
import com.car.car.service.function.FunctionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/function")
public class FunctionController {

    @Resource
    private FunctionService functionService ;

    @RequestMapping("/functionList")
    @RequiresPermissions("functionList")
    public String functionList() {
        //跳到设置功能的页面
        return "function/functionList" ;
    }

    @RequestMapping("/getFunctionList")
    @RequiresPermissions("functionList")
    @ResponseBody
    public List<TreeNode> getFunctionList(@RequestParam(value="id",required = false,defaultValue = "0")int id) {
        //根据层级，获取功能，parentId为0则表示获取一级功能列表
        List<Function> functionList = functionService.getChildrenFunctionList(id) ;

        List<TreeNode> treeNodes = new ArrayList<>() ;
        for(Function function : functionList) {
            TreeNode treeNode = new TreeNode() ;
            treeNode.setId(function.getId()+"");
            treeNode.setName(function.getFunctionName());
            treeNode.setPid(function.getParentId()+"");
            treeNode.setIsParent("true");
            treeNode.setOpen("false");

            treeNodes.add(treeNode) ;
        }

        return treeNodes ;
    }

    @RequestMapping("/getFunctionInfo")
    @RequiresPermissions("functionList")
    @ResponseBody
    public Function getFunctionInfo(int fid) {
        Function function = functionService.getFunctionInfo(fid) ;
        return function ;
    }

    @RequestMapping("/functionAdd")
    @RequiresPermissions("functionAdd")
    @ResponseBody
    public int functionAdd(Function function) {
        int success = functionService.addFunction(function) ;
        return success ;
    }

    @RequestMapping("/functionEdit")
    @RequiresPermissions("functionEdit")
    @ResponseBody
    public int functionEdit(Function function) {
        int success = functionService.updateFunction(function) ;
        return success ;
    }
}
